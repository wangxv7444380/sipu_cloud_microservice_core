package models

import (
	"main/middlewares/public/db"
	"reflect"
	"time"
)

//定义person类型结构
type H_license struct {
	Id	int	`gorm:"primary_key" json:"id"` //许可证,主键
	Type	int	`json:"type"` //许可证类型，1周期 (2计次 3计量)
	Service_id	int	`json:"service_id"` //应用id
	User_id	int	`json:"user_id"` //用户id,使用用户
	Order_id	int	`json:"order_id"` //应用订单id
	Owner_id	int	`json:"owner_id"` //拥有方id,公司id
	Purchase_date	time.Time	`json:"purchase_date"` //购买时间
	Create_date	time.Time	`json:"create_date"` //创建时间
	End_date	time.Time	`json:"end_date"` //失效时间
	Status	int	`json:"status"` //状态 1未激活 2已激活 3已失效
	Metering_json	string	`json:"metering_json"` //计量参数
	Company_id	int	`json:"company_id"` //公司ID
	Power_json	string	`json:"power_json"` //权限JSON

}


func (p *H_license) GetLicenses() (licenses [] H_license ,err error) {
	err = db.DbService.Where(&p).Find(&licenses).Error
	return
}

func (p *H_license) GetLicense(where map[string]interface{}) (license  H_license ,err error) {
	err = db.DbService.Where(&p).Where(where).First(&license).Error
	return
}

func (p *H_license) IsEmpty() bool {
	return reflect.DeepEqual(p, H_license{})
}


