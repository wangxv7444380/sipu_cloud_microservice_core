package models

import "time"

//
type H_account struct {
	Id	int	`gorm:"primary_key" json:"id"` //主键
	Username	string	`json:"username"` //账号，字符串或手机号
	Password	string	`json:"password"` //密码，加密
	Type	int	`json:"type"` //类型，1个人 2公司
	Company_id	int	`json:"company_id"` //所属公司
	User_id	string	`json:"user_id"` //所属用户
	Create_id	int	`json:"create_id"` //创建人
	Create_date	time.Time	`json:"create_date"` //创建日期
	Update_id	int	`json:"update_id"` //更新人
	Update_date	time.Time	`json:"update_date"` //更新时间
	Last_login_client	string	`json:"last_login_client"` //上次登录客户端，app或pc
	Is_able	int	`json:"is_able"` //非删除状态，1正常，0已删除
	Is_allow	int	`json:"is_allow"` //非禁用状态，1正常，0已禁用
	Rb_password	string	`json:"rb_password"` //rabbitmq密码
    User H_user `gorm:"foreignkey:User_id"`
}
