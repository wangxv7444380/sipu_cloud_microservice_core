package models

import "time"

//
type H_user struct {
	Id	int	`gorm:"primary_key" json:"id"` //主键
	Name	string	`json:"name"` //姓名
	Telephone	string	`json:"telephone"` //电话号码
	Avatar	string	`json:"avatar"` //头像
	Email	string	`json:"email"` //邮箱
	Company_id	int	`json:"-"` //公司id
	Role_id	int	`json:"role_id"` //角色ID
	Organization_id	int	`json:"organization_id"` //组织机构id
	Reg_ip	string	`json:"-"` //创建或注册IP
	Last_login_time	int	`json:"-"` //最后登录时间
	Wx_openid	string	`json:"-"` //微信openid
	Last_login_ip	string	`json:"-"` //最后登录IP
	Remark	string	`json:"-"` //备注
	Create_id	int	`json:"-"` //创建人id
	Create_date	time.Time	`json:"-"` //创建或注册时间
	Update_id	int	`json:"-"` //更新人id
	Update_date	time.Time	`json:"-"` //更新时间
	Is_allow	int	`json:"-"` //是否禁用,1正常,0禁用
	Is_able	int	`json:"-"` //是否删除,1正常,0删除
}
