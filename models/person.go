package models

import (
   "main/middlewares/public/db"
)
//定义person类型结构
type Person struct {
   Id        int
   FirstName string
   LastName  string
}


func (p *Person) AddPerson() (id int,err error) {
   err = db.DbSQL.Create(&p).Error
   if err != nil {
      return
   }
   id = p.Id
   return
}
func (p *Person) GetPersons() (persons []Person,err error) {
   persons = make([]Person, 0)
   err = db.DbSQL.Find(&persons).Error
   return
}

func (p *Person) GetPerson() (person Person, err error) {
   err = db.DbSQL.Where("id = ?",p.Id).First(&person).Error
   return
}

func (p *Person) ModPerson() ( err error) {
   err = db.DbSQL.Save(&p).Error
   return
}

func (p *Person) DelPerson() (err error) {
   err = db.DbSQL.Delete(&p).Error
   return
}
