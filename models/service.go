package models

import (
	"reflect"
	"time"
	"main/middlewares/public/db"
)

type H_service struct {
	Id	int	`gorm:"primary_key" json:"id"` //主键，应用id
	Name	string	`json:"name"` //应用名称
	Request_name	string	`json:"request_name"` //应用请求名称
	Icon_pc	string	`json:"icon_pc"` //pc端图标
	Icon_app	string	`json:"icon_app"` //app端图标
	Url_pc	string	`json:"url_pc"` //pc端访问地址
	Url_app	string	`json:"url_app"` //app端访问地址
	Company_id	int	`json:"company_id"` //开发者公司id
	Register_key	string	`json:"-"` //应用注册密钥
	Type_id	string	`json:"type_id"` //应用分类
	Develop_type	int	`json:"develop_type"` //应用类型 1内部 2外部
	Mall_type	int	`json:"mall_type"` //应用类型 1基础 2增值
	Hosts	string	`json:"hosts"` //应用所在服务器域名及端口
	Power_json	string	`json:"-"` //权限json
	Description	string	`json:"description"` //描述(富文本)
	Is_allow	int	`json:"-"` //非禁用状态 1正常 0禁用
	Is_able	int	`json:"-"` //非删除状态 1正常 0删除
	Create_id	int	`json:"-"` //创建人
	Create_date	time.Time	`json:"-"` //创建时间
	Update_id	int	`json:"-"` //更新人
	Update_date	time.Time	`json:"-"` //更新时间
	Metering_json	string	`json:"-"` //计量参数
	Is_free	int	`json:"-"` //是否标准功能(1是 0否)
}

func GetServices (ids []int) ([]*H_service,error)    {
	services := make([]*H_service,0)
	err := db.DbService.Where(ids).Where(" is_able = ? and is_allow = ? ",1,1).Find(&services).Error
	return services,err
}

func (p *H_service) IsEmpty() bool {
	return reflect.DeepEqual(p, H_service{})
}


