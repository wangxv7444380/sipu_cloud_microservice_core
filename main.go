package main

import (
    "main/middlewares/public/db"
    "main/middlewares/public/tools"
    //这里讲db作为go/databases的一个别名，表示数据库连接池
    . "main/router"
    "main/middlewares/public/cache"
)
/*ssh -fNg -p 22 -L 3306:127.0.0.1:3306 root@14.29.207.63*/
/*M9!dH8^gG7%e*/
/*fresh*/
/*swag init*/

func init()  {
    db.ConnectDB()
    cache.CreateCache()
}

func main() {
    //当整个程序完成之后关闭数据库连接
    defer db.DbSQL.Close()
    defer db.DbService.Close()
    defer db.DbUser.Close()
    router := InitRouter()
    if err := router.Run(tools.Conf.Get("port")); err != nil {
        panic(err)
    }
}