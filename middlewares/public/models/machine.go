package models

const (
	MachineStatusRunning = iota + 1
	MachineStatusStopped
)

/*
	程序启动时,自动填写
*/
type Machine struct {
	ID          uint   `gorm:"primary_key" json:"id"`
	IP          string `gorm:"unique;size:56" json:"ip"`
	CPU         int64  `json:"cpu"`          // %
	Memory      int64  `json:"memory"`       // MB
	TotalMemory int64  `json:"total_memory"` // MB

	Status int `json:"status"`
}
