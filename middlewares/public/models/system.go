package models

import "github.com/jinzhu/gorm"

const (
	SystemModeStore = iota
	SystemModeEngine
	SystemModeWorker
)

const (
	SystemNsqHostName = "nsq_host"
)

type System struct {
	gorm.Model

	Name string `json:"name"`
	Vale string `json:"value"`
}
