package models

import "github.com/jinzhu/gorm"

type Columns struct {
	gorm.Model

	SeedID uint  `json:"seed_id"`
	Seed   *Seed `json:"seed"`

	Headers string `json:"headers"`
	Value   string `gorm:"type:text" json:"value"`
}
