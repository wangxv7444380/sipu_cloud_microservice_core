package models

import "github.com/jinzhu/gorm"

const (
	UserAuthNormal = iota
	UserAuthMember
	UserAuthAdministrator = 0xff
)

const (
	UserStatusRegister = iota
	UserStatusNormal
	UserStatusBand
)

type User struct {
	gorm.Model
	Username string `gorm:"unique;size:90" json:"username"`
	Password string `json:"-"`
	Authorization int `gorm:"default(0)" json:"authorization"`
	Avatar   string `json:"avatar"`
	Nickname string `json:"nickname"`
	Phone    string `gorm:"unique;size:56" json:"phone"`
	Status int `json:"status"`
}
