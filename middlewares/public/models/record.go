package models

import "github.com/jinzhu/gorm"

type Record struct {
	gorm.Model

	NotElastic bool

	SeedID uint  `json:"seed_id"`
	Seed   *Seed `json:"seed"`

	Url string `json:"url"`

	// todo: params 由 Param 数据 json序列化而来
	//		保证多个key的顺序不变
	Params       string `json:"params"`
	FunctionName string `json:"function_name"`

	UniqueKey string `gorm:"size:128;unique" json:"unique_key"`

	Error   string `json:"error"`                   // 请求错误时，直接存入 error
	Result  string `gorm:"type:text" json:"result"` // 爬取的所有数据均直接存入
	Remarks string `json:"remarks"`                 // 想要存储的额外数据
}

type Param struct {
	Key   string `json:"key"`
	Value string `json:"value"`
}
