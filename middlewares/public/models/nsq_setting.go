package models

import "github.com/jinzhu/gorm"

const (
	DefaultNsqChannel       = "nsq_default"
	NsqSettingStatusRunning = 0
	NsqSettingStatusPause   = 1
)

// 自生成
type NsqSetting struct {
	gorm.Model

	Status int `json:"status"`

	Host string `json:"host"`

	WorkerID uint    `json:"worker_id"`
	Worker   *Worker `json:"worker"`

	SeedID uint  `json:"seed_id"`
	Seed   *Seed `json:"seed"`

	// topic 为 string 的 seed_id
	Topic string `json:"topic"`
}
