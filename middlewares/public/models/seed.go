package models

import "github.com/jinzhu/gorm"

const (
	SeedStatusWaiting = iota
	SeedStatusRunning
	SeedStatusFinished
)

// todo: 添加 seed 的接口中，需要生成 NsqSetting
type Seed struct {
	gorm.Model

	Loaded bool `json:"loaded"`

	UserID uint  `gorm:"index" json:"user_id"` // 数据所属者
	User   *User `json:"user"`

	Name string `json:"name"`

	Url string `gorm:"index;size:128" json:"url"`

	Prams        string `json:"prams"`         // 使用json存放
	FunctionName string `json:"function_name"` // fetcher 中 使用switch判断使用
}
