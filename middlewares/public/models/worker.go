package models

import "github.com/jinzhu/gorm"

const (
	WorkerRunning = iota
	WorkerPause
	WorkerStop
)

const (
	WorkerStatusKey = "worker_status"
)

/*
	虚拟机器(程序)
*/
type Worker struct {
	gorm.Model

	MachineID uint     `json:"machine_id"`
	Machine   *Machine `json:"machine"`

	Status int `json:"status"`
}
