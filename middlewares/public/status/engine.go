package status

const (
	Normal      = 200
	DefaultError =  201
	LoginError     = 300
	UsernameOrPasswordError   = 400
	ServerError         = 500
	CreateWorkerIDError = 501
)
