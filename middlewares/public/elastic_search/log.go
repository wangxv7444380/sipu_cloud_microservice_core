package elastic_search

import (
	"context"
	"github.com/olivere/elastic"
)

const (
	TypeStorage = "storage"
	TypeEngine  = "engine"
	TypeWorker  = "worker"
)

const (
	LevelDefault = iota
	LevelWarning
	LevelError
)

type WorkerLog struct {
	ID    uint
	Level int
	Log   interface{}
}

type DefaultLog struct {
	Level int
	Log   interface{}
}

type Error struct {
	Error error
	Body  interface{}
}

func Elastic(client *elastic.Client, Type string, log interface{}) error {
	if _, err := client.Index().Index("server_log").Type(Type).BodyJson(log).
		Do(context.Background()); err != nil {
		return err
	}
	return nil
}
