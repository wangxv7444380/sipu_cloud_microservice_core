package elastic_search

import (
	"context"
	"github.com/olivere/elastic"
)

func SaveRecord(client *elastic.Client, index string, _type string, body interface{}) error {
	if _, err := client.Index().
		Index("result_seed_" + _type).
		//Index(index).
		Type("default").
		BodyJson(body).Do(context.Background()); err != nil {
		return err
	}

	return nil
}
