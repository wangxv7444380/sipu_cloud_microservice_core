package db

import (
	"main/middlewares/public/tools"
	"fmt"
	"github.com/gin-gonic/contrib/sessions"
	"github.com/go-redis/redis"
)

var (
	Redis *redis.Client
	//SessionStore sessions.RedisStore
)

func RedisConnect() {
	address := tools.Conf.Get("redis_addr")
	password := tools.Conf.Get("redis_pass")
	port := tools.Conf.Get("redis_port")

	Redis = redis.NewClient(&redis.Options{
		Addr:     address + ":" + port,
		Password: password,
		DB:       0,
	})

	_, err := Redis.Ping().Result()
	if err != nil {
		panic(err)
	}

	fmt.Println("[ redis ] connect success!", address, port)
}

func SessionNewStore() sessions.RedisStore {
	address := tools.Conf.Get("redis_addr")
	password := tools.Conf.Get("redis_pass")
	port := tools.Conf.Get("redis_port")

	var err error
	SessionStore, err := sessions.
		NewRedisStore(100, "tcp", address+":"+port, password)
	if err != nil {
		panic(err)
	}

	SessionStore.Options(sessions.Options{
		Path:   "/",
		MaxAge: 60,
	})

	return SessionStore
}
