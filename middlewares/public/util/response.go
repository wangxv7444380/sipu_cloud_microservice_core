package util

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

/*
*	@param data 1: key
* 	@param data 2: data
 */
func SuccessResponse(ctx *gin.Context, data interface{}) {

	if data != nil {
		ctx.JSON(http.StatusOK, gin.H{
			"status":         http.StatusOK,
			"message":        "success",
			"data": data,
		})
		return
	}
	ctx.JSON(
		http.StatusOK, gin.H{
		"status":  http.StatusOK,
		"message": "success",
	})

}
func FailedResponse(ctx *gin.Context, status int, message string) {
	ctx.JSON(http.StatusOK, gin.H{
		"status":  status,
		"message": message,
	})
}
