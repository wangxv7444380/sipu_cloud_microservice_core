package util

import (
	"github.com/gin-gonic/gin"
	"strconv"
)

func PostFormInt(ctx *gin.Context, key string) (value int, err error) {
	value, err = strconv.Atoi(ctx.PostForm(key))
	if err != nil {
		ctx.JSON(200, gin.H{
			"status":  10999,
			"message": key + ": 输入参数类型错误",
		})
	}
	return
}
