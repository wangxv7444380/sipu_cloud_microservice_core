package util

import (
	"crypto/sha256"
	"encoding/hex"
)

func Sha256(text string) (value string) {
	shaOBJ := sha256.New()
	shaOBJ.Write([]byte(text))
	r := shaOBJ.Sum(nil)
	return hex.EncodeToString(r)
}
