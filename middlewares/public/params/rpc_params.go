package params

import "main/middlewares/public/models"

type Result struct {
	Seeds  []models.Record `json:"seeds"`
	Result []models.Record `json:"result"`
}

type Message struct {
	Status  int    `json:"status"`
	Message string `json:"message"`
}
