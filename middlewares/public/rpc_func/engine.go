package rpc_func

const (
	EnginePushResult  = "Engine.PushResult"
	EngineNewWorkerID = "Engine.NewWorkerID"
)
