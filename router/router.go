package router

import (
    _ "main/docs"
    "github.com/gin-gonic/gin"
    "github.com/swaggo/gin-swagger/swaggerFiles"
    "github.com/swaggo/gin-swagger"
    V1 "main/apis/v1"
    "main/middlewares/public/jwt"
)
func InitRouter() *gin.Engine {
    router := gin.Default()
    router.GET("/", V1.IndexApi)
    router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
    v1 := router.Group("/api/v1/")
    {

        v1.POST("/Login/", V1.Login)
        v1.DELETE("/GetAuth/:id",V1.DelServiceCache)
        v1.DELETE("/Login/:username",V1.DelUserCache)
         v1.Use(jwt.JWTAuth())
        {
          v1.GET("/GetAuth/",V1.GetServiceList)
          v1.GET("/GetAuth/:id",V1.AuthService)
        }
    }
    return router
}
